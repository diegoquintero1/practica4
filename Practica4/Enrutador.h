#define enrutador_h
#include<map>
#include <iostream>
#include <string>
using namespace std;
class Enrutador
{

public:
void imprimirTablaNodos(map<string, map<string,int>>&tablaNodos);
void llenarTablaNodos(map<string, map<string,int>>&tablaNodos, string fileName);
bool verificarNodo(string nodo, map <string,int> &tablaNodos);
string mayuscula(string palabra);
void imprimirTablaFinal(string NodoInicial,string Archivos);
};


