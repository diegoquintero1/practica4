
#include <Enrutador.h>
#include <iostream>
#include <map>
#include <fstream>
#include<time.h>
#include <vector>

using namespace std;


int main()
{
    int comprobar;
    char volver='Y';
    Enrutador routers;
    map <string,int> resultados;
        string nombreArchivo;
    while(volver=='Y'){
    char opcion;
    do{
    cout <<"Quiere elegir el archivo o quiere que se lo de aleatoriamente? (1/2)"<<endl;
    cin >> opcion;
    if (opcion=='1'){
        cout <<"Escriba el nombre del archivo sin .txt) "<<endl;
        cin >>nombreArchivo;
        nombreArchivo+=".txt";
    }
    else if(opcion=='2'){
        srand(time(NULL));
        int numeroArchivo=1+ rand() %((5+1)-1);
        nombreArchivo=to_string(numeroArchivo)+".txt";
        cout <<"El archivo escogido aleatoriamente es: "<<nombreArchivo<<endl;
    }
    else
        cout <<"Escriba una opcion valida."<<endl;
    }
    while (opcion !='1' && opcion!='2');




    map<string, map<string,int>>temporal1;
    map <string,int> tabla;

    routers.llenarTablaNodos(temporal1, nombreArchivo);
    cout <<"Los nodos con sus costos de caminos son:  "<<endl<<endl;
    routers.imprimirTablaNodos(temporal1);
    cout <<endl<<endl;


    //llenar tabla temporal2
    map <string,int>temporal2;

    //llenar tabla de "infinitos" temporales

    resultados.clear();
    int infinito=99999;
    map<string, map<string,int>>::iterator nodos;
    for (nodos=temporal1.begin();nodos!=temporal1.end();nodos++){
        resultados.emplace(nodos->first,infinito);
        temporal2.emplace(nodos->first,infinito);
    }
    bool bandera;
    string nodoInicial;
    do{
    cout <<"Ingrese el Nodo Inicial: "<<endl;
    cin >> nodoInicial;
    nodoInicial=routers.mayuscula(nodoInicial);
    bandera=routers.verificarNodo(nodoInicial,resultados);
    if (bandera==false)
        cout <<"El nodo no existe, intente de nuevo."<<endl;
    }
    while(bandera==false);


    //Borrar el nodo inicial de los nodos restantes
    temporal2.erase(nodoInicial);

    int acumulado=0;
    //Algoritmo para encontrar rutas temporales

    map<string, map<string,int>>::iterator iteratorTabla;
    map<string, int>::iterator iteratorRes;

    iteratorRes=resultados.find(nodoInicial);
    iteratorRes->second=0;


    map <string,int> :: iterator iteratortemporal2;
    iteratorTabla=temporal1.find(nodoInicial);
    map <string,int>::iterator i;   //Iterador del iteradorTabla
    for (i=iteratorTabla->second.begin(); i!= iteratorTabla->second.end();i++){
        iteratortemporal2=temporal2.find(i->first);  //iterador de la tabla de Nodos restantes
        iteratorRes=resultados.find(i->first);  //i->first Claves de los mapas dentro de los mapas
        if (i->second+acumulado < iteratorRes->second){  //Si el valor de cada clave del mapa de mapa, es menor al que se encuentra en resultado
            iteratorRes->second=i->second+acumulado; //Insertar en Tabla temporal de Resultados
            iteratortemporal2->second=i->second+acumulado;
        }

    }
    iteratorTabla=temporal1.find(nodoInicial);
    temporal1.erase(iteratorTabla);


    //Buscar cual es el nodo siguiente

    string nodoSiguiente;
    int numeroMenor;
    map <string,int>::iterator posicionMenor;
    while ( !temporal1.empty()){
        numeroMenor=9999;
        map<string,int>::iterator j;
        for (j=temporal2.begin();j!=temporal2.end();j++){
            if (j->second<numeroMenor){
                numeroMenor=j->second;
                posicionMenor=j;

            }
        }
        acumulado=numeroMenor;
        nodoSiguiente=posicionMenor->first;
        temporal2.erase(posicionMenor);

        //Realizar algoritmo con el nodo ya encontrado

        map<string, map<string,int>>::iterator iteratorTabla;
        map<string, int>::iterator iteratorRes;


        iteratorTabla=temporal1.find(nodoSiguiente);

        map <string,int>::iterator i;
        for (i=iteratorTabla->second.begin(); i!= iteratorTabla->second.end();i++){

            iteratortemporal2=temporal2.find(i->first);  //iterador de la tabla de Nodos restantes
            iteratorRes=resultados.find(i->first);
            if (i->second+acumulado < iteratorRes->second){
                iteratorRes->second=i->second+acumulado;
               iteratortemporal2->second=i->second+acumulado;
}
        }
        iteratorTabla=temporal1.find(nodoSiguiente);
        temporal1.erase(iteratorTabla);

    }


    string nodoFinal;
    do{
    cout <<"Ingrese el Nodo Final: "<<endl;
    cin >> nodoFinal;
    nodoFinal=routers.mayuscula(nodoFinal);
    bandera=routers.verificarNodo(nodoFinal,resultados);
    if (bandera==false)
        cout <<"Ingreso un nodo que no existe, intente de nuevo."<<endl;
    }
    while(bandera==false);
    map <string,int> :: iterator posicionRespuesta;
    posicionRespuesta=resultados.find(nodoFinal);
    cout <<"El costo del camino mas corto desde "<< nodoInicial<<" hasta "<<nodoFinal<<" es de: "<<posicionRespuesta->second<<endl;
        string Nodocomparar;
        int numerofinal,numeroresta,numerocomparar;
        map <string,int> :: iterator Final;
        map <string,map<string,int>>:: iterator Nodofind;
        vector<string>arregloruta;
        map<string, map<string,int>>NodosTabla;
        routers.llenarTablaNodos(NodosTabla, nombreArchivo);
        Final=resultados.find(nodoFinal);
        arregloruta.push_back(nodoFinal);
        numerofinal=Final->second;
        map <string,int>:: iterator j;
        int cont=1;
        string Nodofinal2=nodoFinal;
        while(Nodocomparar!=nodoInicial){
                Nodofind=NodosTabla.find(Nodofinal2);
            for (j=Nodofind->second.begin(); j!= Nodofind->second.end();j++){
                Nodocomparar=j->first;
                numeroresta=j->second;
                Final=resultados.find(Nodocomparar);
                numerocomparar=Final->second;

                if (numerofinal-numeroresta==numerocomparar){
                    arregloruta.push_back(Nodocomparar);
                    Nodofinal2=Nodocomparar;
                    numerofinal=numerocomparar;
                    cont++;
                    break;
                }
            }

        }
        cout<<"Los nodos recooridos para hacer la ruta mas corta fueron: ";
        for(int i=cont-1;i>-1;i--){
            if (i==0){
                cout<<arregloruta[i];
            }
            else{
            cout<<arregloruta[i]<<'-';
            }
        }
        cout<<endl;


    cout<<"Quiere volver a hacerlo?(y/n)"<<endl;
    cin>> volver;
    volver=toupper(volver);
    comprobar=int(volver);
    while(1){
    if (comprobar==89||comprobar==78){
       break;
    }
    else{cout<<"Ingrese un valor valido";}
    }
    }
   char volver2;
   cout<<"Quiere ver la tabla?(y/n)"<<endl;
   cin>> volver2;
   volver2=toupper(volver2);
   comprobar=int(volver2);
   while(1){
   if (comprobar==89||comprobar==78){
      break;
   }
   else{cout<<"Ingrese un valor valido";}
   }
   if(volver2=='Y'){
    string Nodos;
    cout<<"  ";
        map <string,int> :: iterator i;
    for (i=resultados.begin();i!=resultados.end();i++){
        cout <<i->first<<" ";
    }
    cout<<endl;
    for (i=resultados.begin();i!=resultados.end();i++){
            Nodos=i->first;
            routers.imprimirTablaFinal(Nodos,nombreArchivo);

    }
    }


    return 0;
}
