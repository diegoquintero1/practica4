#include <Enrutador.h>
#include <iostream>
#include <map>
#include <fstream>

using namespace std;
void Enrutador:: imprimirTablaNodos(map<string, map<string,int>>&tablaNodos){
    map<string, map<string,int>>::iterator i;
    map <string,int>:: iterator j;
    for (i=tablaNodos.begin();i!=tablaNodos.end();i++){
        cout <<i->first<<":";
        for (j=i->second.begin(); j!= i->second.end();j++){
            cout <<j->first<<"=";
            cout <<j->second<<" ";
        }
        cout <<endl;
    }
}

bool Enrutador::verificarNodo(string nodo, map <string,int> &tablaNodos){
    bool verificacion=false;
    map <string,int>::iterator i;
    for (i=tablaNodos.begin();i!=tablaNodos.end();i++){
        if (nodo==i->first){
            verificacion=true;
            break;
        }
    }
    return verificacion;
}

string Enrutador::mayuscula(string palabra){
    string palabraNueva="";
    for (int i=0;i!=palabra.size();i++){
        palabraNueva+=toupper(palabra[i]);
    }
    return palabraNueva;
}
void Enrutador::imprimirTablaFinal(string Nodos,string Archivo  ){
        map <string,int> resultados;
        string nombreArchivo=Archivo;
    string NodoAux=Nodos;
    map<string, map<string,int>>temporal1;
    map <string,int> tabla;

    llenarTablaNodos(temporal1, nombreArchivo);
    map <string,int>temporal2;
    int infinito=99999;
    map<string, map<string,int>>::iterator nodos;
    for (nodos=temporal1.begin();nodos!=temporal1.end();nodos++){
        resultados.emplace(nodos->first,infinito);
        temporal2.emplace(nodos->first,infinito);
    }
     bool bandera;

    do{
    NodoAux=mayuscula(NodoAux);
    bandera=verificarNodo(NodoAux,resultados);
    if (bandera==false)
        cout <<"El nodo no existe, intente de nuevo."<<endl;
    }
    while(bandera==false);


    //Borrar el nodo inicial de los nodos restantes
    temporal2.erase(NodoAux);

    int acumulado=0;
    //Algoritmo para encontrar rutas temporales

    map<string, map<string,int>>::iterator iteratorTabla;
    map<string, int>::iterator iteratorRes;

    iteratorRes=resultados.find(NodoAux);
    iteratorRes->second=0;


    map <string,int> :: iterator iteratortemporal2;
    iteratorTabla=temporal1.find(NodoAux);
    map <string,int>::iterator i;   //Iterador del iteradorTabla
    for (i=iteratorTabla->second.begin(); i!= iteratorTabla->second.end();i++){
        iteratortemporal2=temporal2.find(i->first);  //iterador de la tabla de Nodos restantes
        iteratorRes=resultados.find(i->first);  //i->first Claves de los mapas dentro de los mapas
        if (i->second+acumulado < iteratorRes->second){  //Si el valor de cada clave del mapa de mapa, es menor al que se encuentra en resultado
            iteratorRes->second=i->second+acumulado; //Insertar en Tabla temporal de Resultados
            iteratortemporal2->second=i->second+acumulado;
        }

    }
    iteratorTabla=temporal1.find(NodoAux);
    temporal1.erase(iteratorTabla);


    //Buscar cual es el nodo siguiente

    string nodoSiguiente;
    int numeroMenor;
    map <string,int>::iterator posicionMenor;
    while ( !temporal1.empty()){
        numeroMenor=9999;
        map<string,int>::iterator j;
        for (j=temporal2.begin();j!=temporal2.end();j++){
            if (j->second<numeroMenor){
                numeroMenor=j->second;
                posicionMenor=j;

            }
        }
        acumulado=numeroMenor;
        nodoSiguiente=posicionMenor->first;
        temporal2.erase(posicionMenor);

        //Realizar algoritmo con el nodo ya encontrado

        map<string, map<string,int>>::iterator iteratorTabla;
        map<string, int>::iterator iteratorRes;


        iteratorTabla=temporal1.find(nodoSiguiente);

        map <string,int>::iterator i;
        for (i=iteratorTabla->second.begin(); i!= iteratorTabla->second.end();i++){

            iteratortemporal2=temporal2.find(i->first);  //iterador de la tabla de Nodos restantes
            iteratorRes=resultados.find(i->first);
            if (i->second+acumulado < iteratorRes->second){
                iteratorRes->second=i->second+acumulado;
               iteratortemporal2->second=i->second+acumulado;
}
        }
        iteratorTabla=temporal1.find(nodoSiguiente);
        temporal1.erase(iteratorTabla);

    }
    cout<<NodoAux<<" ";
     for (i=resultados.begin();i!=resultados.end();i++){
        cout<<i->second<<" ";
        }
     cout<<endl;
     }




void Enrutador:: llenarTablaNodos(map<string, map<string,int>>&tablaNodos, string fileName){
    ifstream archivo(fileName);
    map <string,int> tabla;

    string word="",auxWord="";
    string nodo;
    int numero=0;
    if (archivo.is_open()){
        while (archivo.good()){
            char c;
            while (archivo.get(c)){ //ciclo para obtener cada caracter
                if (c!= ',' && c!=' '&& c!= '\n'){
                    word+=c;

                }
                else if (c==' '){
                    auxWord=word;
                    word="";
                }
                else if (c==','){
                    numero=stoi(word);
                    word="";
                    tabla.emplace(auxWord,numero);
                }
                else if(c=='\n'){
                    nodo=word;
                    word="";
                    tablaNodos.emplace(nodo,tabla);
                    tabla.clear();
                }

            }


        }
    }
    else{
        cout<<"El archivo no se encuentra."<<endl;
    }
    archivo.close();
}
